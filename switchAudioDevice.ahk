#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

F6::
    Run, mmsys.cpl
    WinWait, Sound
    ControlSend, SysListView321, {Down 3}
    ControlClick, &Set Default
    ControlClick, OK
    return

F7::
    Run, mmsys.cpl
    WinWait, Sound
    ControlSend, SysListView321, {Down 1}
    ControlClick, &Set Default
    ControlClick, OK
    return