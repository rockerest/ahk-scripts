﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

if not A_IsAdmin
{
   Run *RunAs "%A_ScriptFullPath%"
   ExitApp
}

^CtrlBreak::
    shell := ComObjCreate( "WScript.Shell" )
    exec := shell.Exec( ComSpec " /C " "sc query synergy" )

    synergyRunning := InStr( exec.StdOut.ReadAll(), "running" )

    if( synergyRunning ){
        Run, %comspec% /c net stop synergy
    }
    else{
        Run, %comspec% /c net start synergy
    }

    return